//
//  Categories.h
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Notes.h"

@interface Categories : NSObject

@property NSString * title;
@property NSMutableArray * notes;

- (instancetype)initWithTitle:(NSString *)title;
- (void)addNote:(Notes *)note;

@end
