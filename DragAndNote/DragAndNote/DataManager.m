//
//  DataManager.m
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "DataManager.h"
#import "Categories.h"
#import "Notes.h"
#import "FileManager.h"

@implementation DataManager

+ (id) getDataManager {
	
	static DataManager *dataManager;
	
	//Garante que vai executar em sequência, caso duas Threads tentem excutar de forma concorrente
	@synchronized  (self) {
		if(dataManager == nil)
			dataManager = [[DataManager alloc] init];
	}
	
	return dataManager;
}

- (instancetype) init {
	
	self = [super init];
	if(self) {
		_m_data = [[NSMutableDictionary alloc]init];
		_dataTableView = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

- (void)createCategoryWithTitle:(NSString *)title {
	
	if(_dataTableView != nil) {
		Categories * category = [[Categories alloc] initWithTitle:title];
		[[[DataManager getDataManager] dataTableView] addObject:category];
	}
}

- (void)createNoteWithTitle:(NSString *)title andNote:(NSString *)note onCategorySection:(NSInteger)section {
	Categories * category;
	Notes * newNote;
	
	category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:section];
	
	newNote = [[Notes alloc] initWithTitle:title andNote:note];
	[category addNote:newNote];
}

- (void)saveNotes {
	NSString * stringToSave = @"";
	
	for(int section = 0; section < [[DataManager getDataManager] dataTableView].count; section++ ) {
		Categories * category = [[[DataManager getDataManager] dataTableView] objectAtIndex:section];
		stringToSave = [stringToSave stringByAppendingString:category.title];
		
		if([[DataManager getDataManager] dataTableView].count > 1)
			stringToSave = [stringToSave stringByAppendingString:@"|"];
		
		for(int row = 0; row < [category notes].count; row++ ) {
			Notes * note = [category.notes objectAtIndex:row];
			stringToSave = [stringToSave stringByAppendingString:note.title];
			stringToSave = [stringToSave stringByAppendingString:@"#@"];
			stringToSave = [stringToSave stringByAppendingString:note.note];
			
			if(row < ([category.notes count] - 1))
				stringToSave = [stringToSave stringByAppendingString:@"#?"];
		}
		
		if(section < ([[DataManager getDataManager] dataTableView].count) - 1)
			stringToSave = [stringToSave stringByAppendingString:@"#!"];
	}
	
	NSLog(stringToSave);
	[FileManager RemoveFileAndSaveString:stringToSave inFile:@"notes.data"];
}

- (void)loadNotes {
	NSString * stringSaved = [FileManager LoadStringFromFile:@"notes.data" useBundle:NO];
	NSLog(stringSaved);
	
	NSArray * allCategories = [stringSaved componentsSeparatedByString:@"#!"];
	
	for(int i = 0; i < allCategories.count; i++) {
		if(allCategories.count > 1) {
			NSArray * category = [[allCategories objectAtIndex:i] componentsSeparatedByString:@"|"];
			[self createCategoryWithTitle:[category objectAtIndex:0]];
			
			if(category.count > 1) {
				if(![[category objectAtIndex:1] isEqualToString:@""]) {
					NSArray * notes = [[category objectAtIndex:1] componentsSeparatedByString:@"#?"];
					
					for(int j = 0; j < notes.count; j++) {
						
						NSArray * note = [[notes objectAtIndex:j] componentsSeparatedByString:@"#@"];
						[self createNoteWithTitle:[note objectAtIndex:0] andNote:[note objectAtIndex:1] onCategorySection:i];
					}
				}
			}
		}
	}
}

@end
