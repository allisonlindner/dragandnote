//
//  NewNoteViewController.h
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewNoteViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UIButton *buttonOk;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;
@property (weak, nonatomic) IBOutlet UILabel *novaNotaLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonSave;

@property NSInteger currentSection;

@property NSString * noteTitle;
@property NSString * note;
@property BOOL editable;

- (IBAction)closeView:(id)sender;
- (IBAction)closeKeyboard:(id)sender;
- (IBAction)saveButton:(id)sender;

@end
