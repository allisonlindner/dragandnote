//
//  NewCategoryViewController.m
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "NewCategoryViewController.h"
#import "DataManager.h"

@interface NewCategoryViewController () {
	
	NSString * titleCategory;
}

@end

@implementation NewCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	UIImage * bg = (UIImage *)[[[DataManager getDataManager] m_data] valueForKey:@"snapshoot"];
	
	[_bgImage setImage:bg];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	titleCategory = textField.text;
	return [textField resignFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	titleCategory = textField.text;
}

-(IBAction)closeView:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButton:(id)sender {
	titleCategory = _titleTextField.text;
	
	if(![titleCategory isEqualToString:@""]) {
		[[DataManager getDataManager] createCategoryWithTitle:titleCategory];
		[[DataManager getDataManager] saveNotes];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

@end
