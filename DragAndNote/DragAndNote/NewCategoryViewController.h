//
//  NewCategoryViewController.h
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCategoryViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

- (IBAction)closeView:(id)sender;

@end
