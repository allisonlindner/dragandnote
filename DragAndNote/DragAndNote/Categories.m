//
//  Categories.m
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "Categories.h"

@implementation Categories

- (instancetype)initWithTitle:(NSString *)title {
	self = [self init];
	
	if(self != nil) {
		self.notes = [[NSMutableArray alloc] initWithCapacity:0];
		self.title = title;
	}
	
	return self;
}

-(void)addNote:(Notes *)note {
	[_notes addObject:note];
}

@end
