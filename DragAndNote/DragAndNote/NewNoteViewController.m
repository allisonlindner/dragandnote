//
//  NewNoteViewController.m
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "NewNoteViewController.h"
#import "DataManager.h"

@interface NewNoteViewController () {
	
	NSString * titleNote;
	NSString * textNote;
}

@end

@implementation NewNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	UIImage * bg = (UIImage *)[[[DataManager getDataManager] m_data] valueForKey:@"snapshoot"];
	
	[_bgImage setImage:bg];
	
	[_contentTextView.layer setBorderColor: [[[UIColor grayColor] colorWithAlphaComponent:0.4] CGColor]];
	[_contentTextView.layer setBorderWidth: 0.5];
	[_contentTextView.layer setCornerRadius:8.0f];
	_contentTextView.textContainerInset = UIEdgeInsetsMake(30.0, 10.0, 10.0, 10.0);
	[_contentTextView.layer setMasksToBounds:YES];
	
	_buttonOk.hidden = true;
	_currentSection = [[DataManager getDataManager] m_section];
	
	if(!_editable) {
		_contentTextView.editable = false;
		_noteTextField.enabled = false;
		_buttonSave.hidden = true;
		_novaNotaLabel.text = @"Consulta";
		
		_contentTextView.text = _note;
		_noteTextField.text = _noteTitle;
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	titleNote = textField.text;
	return [textField resignFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	titleNote = textField.text;
}

- (IBAction)closeView:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeKeyboard:(id)sender {
	textNote = _contentTextView.text;
	[_contentTextView resignFirstResponder];
}

- (IBAction)saveButton:(id)sender {
	
	titleNote = _noteTextField.text;
	textNote = _contentTextView.text;
	if(![titleNote isEqualToString:@""] && ![textNote isEqualToString:@""]) {
		[[DataManager getDataManager] createNoteWithTitle:titleNote andNote:textNote onCategorySection:_currentSection];
		[[DataManager getDataManager] saveNotes];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	_buttonOk.hidden = false;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	_buttonOk.hidden = true;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
