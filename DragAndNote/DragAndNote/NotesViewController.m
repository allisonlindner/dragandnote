//
//  NotesViewController.m
//  DragAndNote
//
//  Created by Allison Lindner on 24/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "NotesViewController.h"
#import "DataManager.h"
#import "NewNoteViewController.h"
#import "NewCategoryViewController.h"
#import "Categories.h"
#import "Notes.h"

@interface NotesViewController () {
	UIPanGestureRecognizer * gestureRecognizer;
	
	UIColor * headerColor;
	UIColor * addCategoryColor;
	CGPoint initialPosition;
	
	BOOL moving;
}

@end

@implementation NotesViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[[DataManager getDataManager] loadNotes];
	
	if([[DataManager getDataManager] dataTableView].count == 0) {
		Categories * category = [[Categories alloc] init];
		category.notes = [[NSMutableArray alloc] initWithCapacity:0];
		category.title = @"Categoria";
		Notes * note = [[Notes alloc] init];
		note.title = @"Nota";
		note.note = @"Primeira nota";
		[category.notes addObject:note];
		
		[[[DataManager getDataManager] dataTableView] addObject:category];
	}
	
	gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveAddButtonWithGestureRecognizer:)];
	[_labelNewNote addGestureRecognizer:gestureRecognizer];
	
	headerColor = [[[self.tableView headerViewForSection:0] contentView] backgroundColor];
	addCategoryColor = [self.labelNewCategory backgroundColor];
	
	[self.tableView setBackgroundColor:[UIColor clearColor]];
	moving = false;
	
	[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(animateDragItRight) userInfo:@"Move Drag" repeats:YES];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
	
}

- (void)viewDidAppear:(BOOL)animated {
	[_tableView reloadData];
	
	for(int section = 0; section < [[DataManager getDataManager] dataTableView].count; section++) {
		[[[self.tableView headerViewForSection:section] contentView] setBackgroundColor:[headerColor colorWithAlphaComponent:0.5]];
	}
	
	initialPosition = self.labelNewNote.center;
	[super performSelector:@selector(showDragItLabel) withObject:@"mostra drag it again" afterDelay:1.0];
}

#pragma mark Definiçao do Gesto de Drag

-(void)moveAddButtonWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
	CGPoint touchLocation = [panGestureRecognizer locationInView:self.view];
	
	_labelNewNote.center = touchLocation;
	
	if(panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
		
		if(CGRectIntersectsRect(self.labelNewNote.frame, self.labelNewCategory.frame)) {
			
			[self.labelNewCategory setBackgroundColor:[UIColor colorWithRed:53/255.0f green:89/255.0f blue:171/255.0f alpha:0.5]];
		} else {
			
			[self.labelNewCategory setBackgroundColor:addCategoryColor];
		}
		
		for(int section = 0; section < [[DataManager getDataManager] dataTableView].count; section++) {
			
			UITableViewHeaderFooterView * tbvCell = [self.tableView headerViewForSection:section];
			
			CGRect labelPosition = CGRectMake(((tbvCell.center.x - self.tableView.contentOffset.x)/2) - 80,
										  tbvCell.center.y + 55
										  - self.tableView.contentOffset.y, tbvCell.frame.size.width + 120, tbvCell.frame.size.height);
			
			if(CGRectIntersectsRect(self.labelNewNote.frame, labelPosition)) {
				
				[[[self.tableView headerViewForSection:section] contentView] setBackgroundColor:[UIColor colorWithRed:175/255.0f green:220/255.0f blue:86/255.0f alpha:0.5]];
			} else {
				
				[[[self.tableView headerViewForSection:section] contentView] setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.2]];
			}
		}
		
		moving = true;
	}
	
	if(panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
		
		[_labelDragIt setAlpha:0.0];
		
		[self.labelNewCategory setBackgroundColor:addCategoryColor];
		
		for(int section = 0; section < [[DataManager getDataManager] dataTableView].count; section++) {
			[[[self.tableView headerViewForSection:section] contentView] setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.2]];
			[[[self.tableView headerViewForSection:section] contentView] setOpaque:false];

		}
		
		[[self labelNewCategory] setAlpha:1.0];
		[self.tableView reloadData];
		moving = true;
	}
	
	//Quando termina de mover o botão
	if(panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
		
		for(int section = 0; section < [[DataManager getDataManager] dataTableView].count; section++) {

			UITableViewHeaderFooterView * tbvCell = [self.tableView headerViewForSection:section];
			
			CGRect labelPosition = CGRectMake(((tbvCell.center.x - self.tableView.contentOffset.x)/2) - 80,
											  tbvCell.center.y + 55
											  - self.tableView.contentOffset.y, tbvCell.frame.size.width + 120, tbvCell.frame.size.height);
			
			if(CGRectIntersectsRect(self.labelNewNote.frame, labelPosition)) {
				
				//Tira a Snpashoot da tela para fazer o effeito de blur
				[[[DataManager getDataManager] m_data] setValue:[self snapshoot] forKey:@"snapshoot"];
				
				//Chama a proxima tela usando o Efeito de Cross Dissolve
				UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
				NewNoteViewController *newNoteViewController = (NewNoteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"newNote"];
				newNoteViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
				[[DataManager getDataManager] setM_section:section];
				newNoteViewController.editable = true;
				[self presentViewController:newNoteViewController animated:YES completion:nil];
				break;
			}
			moving = false;
		}
		
		if(CGRectIntersectsRect(self.labelNewNote.frame, self.labelNewCategory.frame)) {
			
			//Tira a Snpashoot da tela para fazer o effeito de blur
			[[[DataManager getDataManager] m_data] setValue:[self snapshoot] forKey:@"snapshoot"];
			
			//Chama a proxima tela usando o Efeito de Cross Dissolve
			UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
			NewCategoryViewController *newCategoryViewController = (NewCategoryViewController *)[storyboard instantiateViewControllerWithIdentifier:@"newCategory"];
			newCategoryViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
			[self presentViewController:newCategoryViewController animated:YES completion:nil];
		}
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDelay:0.0];
		[UIView setAnimationDuration:0.5];
		[UIView setAnimationBeginsFromCurrentState:TRUE];
		
		self.labelNewNote.center = initialPosition;

		[UIView commitAnimations];
		
		[self.tableView reloadData];
		[[self labelNewCategory] setAlpha:0.2];
		
		[super performSelector:@selector(showDragItLabel) withObject:@"mostra drag it again" afterDelay:0.8];
	}
}

#pragma mark Take Snapshot

-(UIImage *)snapshoot {
	
	self.labelNewNote.hidden = true;

	UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0);

	[self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];

	UIImage *copied = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	self.labelNewNote.hidden = false;
	
	return copied;
}

#pragma mark Animação para indicar o movimento de Drag
- (void) animateDragItRight {
	if(!moving) {
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDelay:3.0];
		[UIView setAnimationDuration:0.5];
		[UIView setAnimationBeginsFromCurrentState:TRUE];
		_labelNewNote.center = CGPointMake(_labelNewNote.center.x, _labelNewNote.center.y + 15);
		[UIView commitAnimations];
		
		[super performSelector:@selector(animateDragItLeft) withObject:@"move DragIt" afterDelay:0.5];
	}
}

- (void) animateDragItLeft {
	if(!moving) {
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDelay:3.0];
		[UIView setAnimationDuration:0.5];
		[UIView setAnimationBeginsFromCurrentState:TRUE];
		_labelNewNote.center = CGPointMake(_labelNewNote.center.x, _labelNewNote.center.y - 15);
		[UIView commitAnimations];
	}
}

- (void) showDragItLabel {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDelay:0.0];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationBeginsFromCurrentState:TRUE];
	
	[_labelDragIt setAlpha:0.3];
	
	[UIView commitAnimations];
}

#pragma mark UITabelViewDelegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	if (tableView == self.searchDisplayController.searchResultsTableView)
		return 1;
	else
		return [[DataManager getDataManager] dataTableView].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		
		return _searchResults.count;
		
	} else {
		
		Categories * category;
		
		category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:section];
		
		if(category.notes.count == 0)
			return 1;
		
		return category.notes.count;
	}
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NoteTableViewCell * cell = (NoteTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"NoteCell"];
	
	if (cell == nil) {
		cell = (NoteTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"NoteCell"];
	}
	
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		Notes * note;
		
		if(_searchResults.count > 0) {
			note = (Notes *)[_searchResults objectAtIndex:indexPath.row];
			
			cell.labelText.text = note.title;
			cell.labelContent.text = note.note;
		} else {
			cell.labelText.text = @"";
			cell.labelContent.text = @"";
		}
	} else {
		
		Notes * note;
		Categories * category;
		
		category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:indexPath.section];
		if(category.notes.count != 0) {
			note = (Notes *)[category.notes objectAtIndex:indexPath.row];
		
			cell.labelText.text = note.title;
			cell.labelContent.text = note.note;
		} else {
			cell.labelText.text = @"";
			cell.labelContent.text = @"";
		}
	}
	
	return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	Categories * category;
	
	if (tableView != self.searchDisplayController.searchResultsTableView) {
		
		category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:section];
	}
	
	return category.title;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 51;
}


#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	
	_searchResults = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int section = 0; section < [[[DataManager getDataManager] dataTableView] count]; section++) {
		for(int row = 0; row < [[[[[DataManager getDataManager] dataTableView] objectAtIndex:section] notes] count]; row++) {
			Categories * category;
			category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:section];
			
			Notes * note = (Notes *)[category.notes objectAtIndex:row];
			
			if([[note.title lowercaseString] containsString:[searchString lowercaseString]]) {
				[_searchResults addObject:note];
			} else if([[note.note lowercaseString] containsString:[searchString lowercaseString]]) {
				[_searchResults addObject:note];
			}
		}
	}
	
	return YES;
}

#pragma mark Index

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	NSMutableArray * indexes = [[NSMutableArray alloc] initWithCapacity:0];
	
	for(int section = 0; section < [[[DataManager getDataManager] dataTableView] count]; section++) {
		Categories * category;
		category = (Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:section];
		
		[indexes addObject:category.title];
	}
	
	return indexes;
}

#pragma mark On Selec Row

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == self.searchDisplayController.searchResultsTableView) {
		//Tira a Snpashoot da tela para fazer o effeito de blur
		[[[DataManager getDataManager] m_data] setValue:[self snapshoot] forKey:@"snapshoot"];
		
		//Chama a proxima tela usando o Efeito de Cross Dissolve
		UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		NewNoteViewController *newNoteViewController = (NewNoteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"newNote"];
		newNoteViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		
		newNoteViewController.editable = false;
		newNoteViewController.noteTitle = ((Notes *)[_searchResults objectAtIndex:indexPath.row]).title;
		newNoteViewController.note = ((Notes *)[_searchResults objectAtIndex:indexPath.row]).note;
		
		[self presentViewController:newNoteViewController animated:YES completion:nil];
	} else {
		
		if([(Categories *)[[[DataManager getDataManager] dataTableView] objectAtIndex:indexPath.section] notes].count > 0) {
			
			//Tira a Snpashoot da tela para fazer o effeito de blur
			[[[DataManager getDataManager] m_data] setValue:[self snapshoot] forKey:@"snapshoot"];
			
			//Chama a proxima tela usando o Efeito de Cross Dissolve
			UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
			NewNoteViewController *newNoteViewController = (NewNoteViewController *)[storyboard instantiateViewControllerWithIdentifier:@"newNote"];
			newNoteViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
			
			newNoteViewController.editable = false;
			newNoteViewController.noteTitle = ((Notes *)[[[[[DataManager getDataManager] dataTableView] objectAtIndex:indexPath.section] notes] objectAtIndex:indexPath.row]).title;
			newNoteViewController.note = ((Notes *)[[[[[DataManager getDataManager] dataTableView] objectAtIndex:indexPath.section] notes] objectAtIndex:indexPath.row]).note;
			
			[self presentViewController:newNoteViewController animated:YES completion:nil];
		}
	}
}

@end
