//
//  Notes.m
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "Notes.h"

@implementation Notes

- (instancetype)initWithTitle:(NSString *)title andNote:(NSString *)note {
	self = [self init];
	
	if(self != nil) {
		self.title = title;
		self.note = note;
	}
	
	return self;
}

@end
