//
//  NoteTableViewCell.h
//  DragAndNote
//
//  Created by Allison Lindner on 24/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UILabel *labelContent;

@end
