//
//  DataManager.h
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (id) getDataManager;

@property NSMutableDictionary * m_data;
@property NSMutableArray * dataTableView;
@property int m_section;

- (void)createCategoryWithTitle:(NSString *)title;
- (void)createNoteWithTitle:(NSString *)title andNote:(NSString *)note onCategorySection:(NSInteger)section;

- (void)saveNotes;
- (void)loadNotes;

@end
