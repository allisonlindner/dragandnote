//
//  FileManager.h
//  ManupulandoArquivos
//
//  Created by Mark Joselli on 3/13/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

+(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle;

+(BOOL) SaveString:(NSString*)string inFile:(NSString *)file;

+(BOOL)RemoveFileAndSaveString:(NSString *)string inFile:(NSString *)file;

@end
