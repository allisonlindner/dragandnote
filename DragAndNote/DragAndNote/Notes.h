//
//  Notes.h
//  DragAndNote
//
//  Created by Allison Lindner on 25/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notes : NSObject

@property NSString * title;
@property NSString * note;

- (instancetype)initWithTitle:(NSString *)title andNote:(NSString *)note;

@end
