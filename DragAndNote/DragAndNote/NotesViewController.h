//
//  NotesViewController.h
//  DragAndNote
//
//  Created by Allison Lindner on 24/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteTableViewCell.h"

@interface NotesViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UILabel *labelNewNote;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelNewCategory;
@property (weak, nonatomic) IBOutlet UILabel *labelDragIt;

@property NSMutableArray *searchResults;

@end
